<?php
$this->pageTitle=Yii::app()->name. ' - Manage Venue Information';
/* @var $this ShiftsController */
/* @var $model Shifts */

$this->breadcrumbs=array(
    'Manage Venue Information',
);

$this->menu=array(
    array('label'=>'List Shifts', 'url'=>array('index')),
    array('label'=>'Create Shifts', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#shifts-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<p>
    <a href="<?php echo Yii::app()->createUrl('venue/create')?>" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Create New Venue</a>
</p>

<div class="box box-solid box-default">
    <div class="box-header">
        <h3 class="box-title">Manage Venue Information</h3>
    </div>
    <div class="box-body no-padding">
        <?php $this->widget('booster.widgets.TbGridView', array(
            'id'=>'shifts-grid',
            'type'=>'bordered',
            'ajaxUpdate'=>false,
            'dataProvider'=>$model->search(),
//            'filter'=>$model,
            'columns'=>array(
                array(
                    'header'=>'#',
                    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
                    'htmlOptions'=>array('style'=>'width:30px;'),
                ),
                'name',
                array(
                    'name'=>'updated_by',
                    'value'=>'(isset($data->updatedBy))? $data->updatedBy->profile->first_name:""',
                ),
                'timestamp',
                array(
                    'htmlOptions'=>array('nowrap'=>'nowrap'),
                    'class'=>'CButtonColumn',
                    'template'=>'{view} {update} {delete}',
                    'buttons'=>array(
                        'view'=>array(
                            'label'=>'Lihat',
                            'options'=>array('class'=>'btn btn-primary btn-sm'),
                            'imageUrl'=>false,
                        ),
                        'update'=>array(
                            'label'=>'Kemaskini',
                            'options'=>array('class'=>'btn btn-warning btn-sm'),
                            'imageUrl'=>false,
                        ),
                        'delete'=>array(
                            'label'=>'Hapus',
                            'options'=>array('class'=>'btn btn-danger btn-sm'),
                            'imageUrl'=>false,
                        ),
                    ),
                ),

            ),
        )); ?>
    </div>
</div>


