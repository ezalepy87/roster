<?php
/* @var $this WorkerController */
/* @var $model TblUsers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'tbl-users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <!-- input username -->
		<?php echo $form->textFieldGroup($model,'username',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('size'=>20,'maxlength'=>20)
            )
        )); ?>

        <!-- input email -->
		<?php echo $form->textFieldGroup($model,'email',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('size'=>60,'maxlength'=>128),
            ),
        )); ?>

        <!-- input firstname -->
        <?php echo $form->textFieldGroup($profile, 'first_name', array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('maxlength'=>255),
            ),
        ));?>

        <!-- input nokp -->
        <?php echo $form->textFieldGroup($profile, 'nokp', array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('maxlength'=>12),
            ),
        ));?>



	<div class="form-actions">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->