<?php

/**
 * This is the model class for table "schedules".
 *
 * The followings are the available columns in table 'schedules':
 * @property integer $id
 * @property integer $shift_id
 * @property integer $user_id
 * @property string $date
 * @property string $timestamp
 * @property integer $updated_by
 * @property integer $venue_id
 *
 * The followings are the available model relations:
 * @property Users $updatedBy
 * @property Shifts $shift
 * @property Users $user
 */
class Schedules extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'schedules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shift_id, user_id, updated_by, venue_id', 'numerical', 'integerOnly'=>true),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, shift_id, user_id, date, timestamp, updated_by, venue_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'updatedBy' => array(self::BELONGS_TO, 'User', 'updated_by'),
			'shift' => array(self::BELONGS_TO, 'Shifts', 'shift_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'venue' => array(self::BELONGS_TO, 'Venue', 'venue_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shift_id' => 'Shift',
			'user_id' => 'User',
			'date' => 'Date',
			'timestamp' => 'Timestamp',
			'updated_by' => 'Updated By',
			'venue_id' => 'Zone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('venue_id',$this->venue_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Schedules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Before save, insert user id
     * @return mixed
     */
    public function beforeSave() {

        $this->updated_by = Yii::app()->user->id;

        return parent::beforeSave();
    }
}
