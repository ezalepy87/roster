<?php  

class BaseFunctions{

    public static function monthList()
    {
        $month = array(
            '01'=>'January',
            '02'=>'February',
            '03'=>'March',
            '04'=>'April',
            '05'=>'May',
            '06'=>'June',
            '07'=>'July',
            '08'=>'August',
            '09'=>'September',
            '10'=>'October',
            '11'=>'November',
            '12'=>'December',
        );

        return $month;
    }

    public static function yearList()
    {
        $current = date('Y');
        $start = $current-5;

        for($i = $start ; $i <= $current ; $i++){
            $data[$i] = $i;
        }
        return $data;
    }

    /**
     * @param $month
     * @return string
     */
    public static function monthDetails($month)
    {
        switch($month)
        {
            case '01' : $month = "January";break;
            case '02' : $month = "February";break;
            case '03' : $month = "March";break;
            case '04' : $month = "April";break;
            case '05' : $month = "Mei";break;
            case '06' : $month = "June";break;
            case '07' : $month = "July";break;
            case '08' : $month = "August";break;
            case '09' : $month = "September";break;
            case '10' : $month = "October";break;
            case '11' : $month = "November";break;
            case '12' : $month = "December";break;
        }
        return $month;
    }

	/**
	 * Return Icon syntax
	 *
	 * @return void
	 * @author haezal musa
	 **/
	public static function icon($value="")
	{
		return "<span class='glyphicon glyphicon-".$value."'></span>";
	}

	/**
	 * Get total branchs by brand id
	 *
	 * @return integer
	 * @author haezal
	 **/
	public static function totalBranchs($id)
	{
		$branchs=Branchs::model()->findAllByAttributes(array('brand_id'=>$id));
		return count($branchs);
	}
}
?>