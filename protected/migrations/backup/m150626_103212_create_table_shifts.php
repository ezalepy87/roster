<?php

class m150626_103212_create_table_shifts extends CDbMigration
{
    public function up()
    {
        $this->createTable('shifts', array(
            'id'=>'pk',
            'name'=>'string',
            'starttime'=>'time',
            'endtime'=>'time',
            'timestamp'=>'timestamp',
            'updated_by'=>'integer',
        ));

        $this->addForeignKey('shifts_updated_by_foreign', 'shifts', 'updated_by', 'tbl_users', 'id', 'SET NULL');
    }

    public function down()
    {
        $this->dropTable('shifts');
        return false;
    }
}