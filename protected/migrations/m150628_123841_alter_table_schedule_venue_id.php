<?php

class m150628_123841_alter_table_schedule_venue_id extends CDbMigration
{
	public function up()
	{
        $this->addColumn('schedules', 'venue_id', 'integer');
        $this->addForeignKey('schedules_venue_id_foreign', 'schedules', 'venue_id', 'venue', 'id', 'SET NULL');
	}

	public function down()
	{
        $this->dropColumn('schedules', 'venue_id');
        $this->dropForeignKey('schedules_venue_id_foreign', 'schedules');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}