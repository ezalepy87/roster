# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.38)
# Database: roster
# Generation Time: 2015-06-28 13:39:24 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table AuthAssignment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `AuthAssignment`;

CREATE TABLE `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `AuthAssignment` WRITE;
/*!40000 ALTER TABLE `AuthAssignment` DISABLE KEYS */;

INSERT INTO `AuthAssignment` (`itemname`, `userid`, `bizrule`, `data`)
VALUES
	('Admin','1',NULL,'N;'),
	('supervisor','32',NULL,'N;'),
	('worker','33',NULL,'N;'),
	('worker','36',NULL,'N;'),
	('worker','37',NULL,'N;'),
	('worker','38',NULL,'N;'),
	('worker','39',NULL,'N;'),
	('worker','40',NULL,'N;'),
	('worker','41',NULL,'N;'),
	('worker','42',NULL,'N;'),
	('worker','43',NULL,'N;'),
	('worker','44',NULL,'N;'),
	('worker','45',NULL,'N;'),
	('worker','46',NULL,'N;'),
	('worker','47',NULL,'N;'),
	('worker','48',NULL,'N;'),
	('worker','49',NULL,'N;'),
	('worker','50',NULL,'N;'),
	('worker','51',NULL,'N;'),
	('worker','52',NULL,'N;'),
	('worker','53',NULL,'N;'),
	('worker','54',NULL,'N;'),
	('worker','55',NULL,'N;'),
	('worker','56',NULL,'N;'),
	('worker','57',NULL,'N;'),
	('worker','58',NULL,'N;'),
	('worker','59',NULL,'N;'),
	('worker','60',NULL,'N;'),
	('worker','61',NULL,'N;'),
	('worker','62',NULL,'N;'),
	('worker','63',NULL,'N;'),
	('worker','64',NULL,'N;'),
	('worker','65',NULL,'N;'),
	('worker','66',NULL,'N;'),
	('worker','67',NULL,'N;'),
	('worker','68',NULL,'N;'),
	('worker','69',NULL,'N;'),
	('worker','70',NULL,'N;');

/*!40000 ALTER TABLE `AuthAssignment` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
