# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.38)
# Database: roster
# Generation Time: 2015-06-28 13:29:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username` (`username`),
  UNIQUE KEY `user_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`)
VALUES
	(1,'admin','0192023a7bbd73250516f069df18b500','webmaster@example.com','8c14807ac6ece3a87e77eccfef880bee',1,1,'2015-03-28 13:14:56','2015-06-26 19:01:31'),
	(32,'supervisor','81dc9bdb52d04dc20036dbd8313ed055','supervisor@gmail.com','2baf7928c019fbb5718a0aa9259f3231',0,1,'2015-06-26 18:58:14','2015-06-28 13:59:42'),
	(36,'worker','b639b24562d8b96439aaf1f1ee3acb48','worker@gmail.com','7e07f68c8fc6f5c5ad6c8ca850832359',0,1,'2015-06-27 12:03:23','0000-00-00 00:00:00'),
	(37,'worker2','b639b24562d8b96439aaf1f1ee3acb48','worker2@gmail.com','567ac579b3f02b588acc3eedfbba76d9',0,1,'2015-06-27 12:04:26','0000-00-00 00:00:00'),
	(38,'worker3','107bd7df57033afd773b19610be418fc','worker3@gmail.com','f97662dde1f7e30fdf4bd22398bdce3e',0,1,'2015-06-27 12:04:46','0000-00-00 00:00:00'),
	(39,'worker4','55bdd5188be6117565ca45b8eeafba59','worker4@gmail.com','60160e40bbf01b1a4cef4d8ffbb88ae0',0,1,'2015-06-27 18:03:10','0000-00-00 00:00:00'),
	(40,'worker5','4ab70718a49d50df8d8c19da1faf12a4','worker5@gmail.com','5474d04df52781081760b0693fc5f313',0,1,'2015-06-27 18:03:49','0000-00-00 00:00:00'),
	(41,'worker6','23d82b6f33cf512b925f1aacfc6d491b','worker6@gmail.com','329dd040deb2a8afaecf4803fb3a478f',0,1,'2015-06-27 18:04:05','0000-00-00 00:00:00'),
	(42,'worker7','ecdcaefc41ca51e02c1c02de5cd1f367','worker7@gmail.com','809cbcdfc00cc8c086c390bde20f51b0',0,1,'2015-06-27 18:14:55','0000-00-00 00:00:00'),
	(43,'worker8','189ead783accbc922747940b134d03d9','worker8@gmail.com','bed56e718da33490c8486985c5cff476',0,1,'2015-06-27 18:15:24','0000-00-00 00:00:00'),
	(44,'worker9','d2727a4585fc1bfd79f23e95852991d7','worker9@gmail.com','db9379aab2199d12da8ac38fb86bdbfd',0,1,'2015-06-27 18:16:20','0000-00-00 00:00:00'),
	(45,'worker10','9d259ba10185f2726f1b8ad14c9145d0','worker10@gmail.com','dec5cc821cedc33c98c25791c5d71ce6',0,1,'2015-06-27 18:16:38','0000-00-00 00:00:00'),
	(46,'worker11','45d5c601e515ce31ffdfd6c88371f3de','worker11@gmail.com','3622c44974722e12f62947ed758b26bf',0,1,'2015-06-27 18:16:56','0000-00-00 00:00:00'),
	(47,'worker12','695c6b7c88bb33e7a5216afa8d6df69b','worker12@gmail.com','855294d9814030aec8b898dadc91630d',0,1,'2015-06-27 18:17:30','0000-00-00 00:00:00'),
	(48,'worker13','a9cbe6c2c56d741c6a28a260b47710b6','worker13@gmail.com','e66c3b3861c6eed795aaadeb9e00c6eb',0,1,'2015-06-27 18:17:57','0000-00-00 00:00:00'),
	(49,'worker14','9c3e54994585c9b788611007ac48004c','worker14@gmail.com','dbb75b469eb48a5e4ad9fc86d1a42ec2',0,1,'2015-06-27 18:18:15','0000-00-00 00:00:00'),
	(50,'worker15','29782c528e49235940e78a76500ff886','worker15@gmail.com','992621033eaaed9461cdc40bd8c4a431',0,1,'2015-06-27 18:18:28','0000-00-00 00:00:00'),
	(51,'worker16','2d1b42e699401e69bed47f646bd4e297','worker16@gmail.com','cd825c5657d2e4cdb003f92ecb12dbf0',0,1,'2015-06-27 18:19:04','0000-00-00 00:00:00'),
	(52,'worker17','0084dd960271dd3706b120caebc3018a','worker17@gmail.com','0652df9307b6d8c83d419072ffd2730c',0,1,'2015-06-27 18:20:16','0000-00-00 00:00:00'),
	(53,'worker18','b697680fdeaaee64f156dfa202c3ee6c','worker18@gmail.com','b52207fd1e0d0911afd429f071f8f54f',0,1,'2015-06-27 18:20:32','0000-00-00 00:00:00'),
	(54,'worker19','a3ce6508278a5b6d345749d9b658a9d9','worker19@gmail.com','5908245a69a08101d8f8d94e9b9d9e4b',0,1,'2015-06-27 18:20:45','0000-00-00 00:00:00'),
	(55,'worker20','63fa4f07de98ac77c97c00fa9d92f751','worker20@gmail.com','cddd7462f4a683db9ccb59133ef0f34f',0,1,'2015-06-27 18:21:10','0000-00-00 00:00:00'),
	(56,'worker21','4ce8b2acaa6388002d121fd02ccbdd8b','worker21@gmail.com','57e9f4a026d7990f3dcbfbacd50fd23e',0,1,'2015-06-28 04:26:35','0000-00-00 00:00:00'),
	(57,'worker22','f9b45cefbc9d5d154e26fd97d79ff8a5','worker22@gmail.com','8e95af132982c1465556f1ce49e0df3e',0,1,'2015-06-28 04:26:59','0000-00-00 00:00:00'),
	(58,'worker23','86c2affc603925424bcac458aed61228','worker23@gmail.com','2d5bb5f0be40f9472db25138ebf19551',0,1,'2015-06-28 04:27:14','0000-00-00 00:00:00'),
	(59,'worker24','fe019806932430bea43189021d1da754','worker24@gmail.com','c518e459325e141d330d3ae7f3098e62',0,1,'2015-06-28 04:27:34','0000-00-00 00:00:00'),
	(60,'worker25','62879235ba2df21964e7104e1b5f0455','worker25@gmail.com','35925493c2e269bedf63f8708dba71d4',0,1,'2015-06-28 04:27:55','0000-00-00 00:00:00'),
	(61,'worker26','a5979d34d8640c852067e456cb3c45e7','worker26@gmail.com','1bdba5d6b0dcb960c867fef516114a64',0,1,'2015-06-28 04:28:14','0000-00-00 00:00:00'),
	(62,'worker27','837ea621a04e1ece62cb2fdad7f5cd6e','worker27@gmail.com','8b8f55546b2ad62ce03d3e83f6cbd1d0',0,1,'2015-06-28 04:28:28','0000-00-00 00:00:00'),
	(63,'worker28','fa664ed62f5e865628793c8ad750a5c2','worker28@gmail.com','4d64c12526f6d730d2e881906f01230d',0,1,'2015-06-28 04:28:43','0000-00-00 00:00:00'),
	(64,'worker29','524e9c195dd75b15f20e3b26f5199f29','worker29@gmail.com','5871b73c8463e71da7828e87bf51acd9',0,1,'2015-06-28 04:29:03','0000-00-00 00:00:00'),
	(65,'worker30','ec86c397e041a17122cc75548a79225c','worker30@gmail.com','e3b97de9e8fd18a8357cb28015a4546a',0,1,'2015-06-28 04:29:17','0000-00-00 00:00:00'),
	(66,'worker31','9c069f66d045748b7d996ff285ff2ff4','worker31@gmail.com','1bff43bface5d6a87e5fd2ec25c06059',0,1,'2015-06-28 04:29:32','0000-00-00 00:00:00'),
	(67,'worker32','b0a06bdcf2a6ee9658eb19ae3465f690','worker32@gmail.com','185e073b9a4367c2a65996820a960fcb',0,1,'2015-06-28 04:29:50','0000-00-00 00:00:00'),
	(68,'worker33','c7dec867a98397241e3d11838b666619','worker33@gmail.com','1c17876e358acf7ca29de40a00f28675',0,1,'2015-06-28 04:30:00','0000-00-00 00:00:00'),
	(69,'worker34','3f641ccc778eb07e9ec0b884646a203a','worker34@gmail.com','4c3f62fc3f78649eca1043fe8044ac3c',0,1,'2015-06-28 04:30:14','0000-00-00 00:00:00'),
	(70,'worker35','d336d871e96d9c018064abcc32376950','worker35@gmail.com','6c38675e523d7637aca272e3d08d3a1f',0,1,'2015-06-28 04:30:37','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
